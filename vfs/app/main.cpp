////////////////////////////////////////////////////////////////////////////////

/**
 * @file
 
 * @brief Entrypoint for the Virtual File System application.

*/

////////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "Command.hpp"
#include "CommandParser.hpp"
#include "FileSystem.hpp"

int main() {
    std::string input;
    shared_ptr<ICommandParser> parser = std::make_shared<CommandParser>();
    shared_ptr<FileSystem>     fileSystem = std::make_shared<FileSystem>();

    parser->setFileSystem(fileSystem);

    cout << "-_-_-_-_-_-_- | - Virtual Filesystem - | -_-_-_-_-_-_-" << std::endl;
    while (1) {
        cout << "<VFS>" << fileSystem->currentWorkingDir->absolutePath << ":$ ";
        
        getline(std::cin, input);

        if (input == "quit")
            break;
        if (input == "")
            continue;
        
        parser->parseCommand(input);
    }
    return 0;
}
