#include "FileSystem.hpp"
#include <iostream>

// constructor
FileSystem::FileSystem() {
	rootDir = std::make_shared<FileSystemComponent>("root");
    rootDir->parent = 0;
    currentWorkingDir = rootDir;
    rootDir->absolutePath = rootDir->name;
}

// destructor
FileSystem::~FileSystem() {
    // delete root;
    // cout << "FileSystem destruction" << std::endl;
}