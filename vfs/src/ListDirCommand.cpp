#include "../inc/ListDirCommand.hpp"

map<int, char> types = {
    {DIRECTORY, 'd'},
    {_FILE, 'f'},
    {BINARY, 'e'}};

void ListDirCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> fs) {
    if (arguments.size() > 1) {
        cout << "ls: too many arguments. Will be updated with linux standard" << endl;
        return;
    }

    if (fs->currentWorkingDir->child.size() == 0) {
        return;
    }

    cout << "TYPE\tNAME" << endl;
    
    for (auto i : fs->currentWorkingDir->child) {
        cout << "  " << types[i->componentType] << "\t" << i->name << "    " << endl;
    }
}