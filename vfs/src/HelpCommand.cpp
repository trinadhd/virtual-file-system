#include "../inc/HelpCommand.hpp"

void HelpCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> fs) {
    std::cout << "ls" << std::endl;
    std::cout << "mkdir <names of directories to be created with space separated" << std::endl;
    std::cout << "cd <directory_name>" << std::endl;
    std::cout << "rmdir <names of directories to be removed with space separated" << std::endl;
}