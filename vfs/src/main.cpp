#include <iostream>
#include <string>
#include <vector>
#include "../Interfaces/IFileSystem.hpp"
#include "../Interfaces/ICommandParser.hpp"

std::vector<std::string> command()
int main() {
    std::string inputCommand;
    std::getline(std::cin, inputCommand);
    std::vector<std::string> tokens;
    std::stringstream check1(inputCommand);
    std::string temp;
    while(getline(check1,temp,' ')){
        tokens.push_back(temp);
    }
    ICmdParser parser(tokens);
    return 0;
}