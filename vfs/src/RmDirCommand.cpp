#include "../inc/RmDirCommand.hpp"

void RmDirCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> fs) {
#define CHILD fs->currentWorkingDir->child
    bool isDirFound = false;
    
    if (arguments.size() < 2) {
        cout << "rmdir: missing operand" << endl;
        return;
    }

    for (int j = 1; j < arguments.size(); j++) {
        for (int i = 0; i < CHILD.size(); i++) {
            if (CHILD[i]->name == arguments[j]) {
                if (CHILD[i]->componentType != DIRECTORY) {
                    cout << arguments[j] << " is not a directory " << endl;
                    continue;
                };

                isDirFound = true;
                CHILD.erase(CHILD.begin() + i);
                cout << arguments[j] << " removed " << endl;
            }
        }
        if (!isDirFound) {
            cout << arguments[j] << " not found" << endl;
        }
    }
}