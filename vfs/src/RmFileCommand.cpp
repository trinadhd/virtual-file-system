#include "../inc/RmFileCommand.hpp"

#include <bits/stdc++.h>

#include <iostream>
#include <string>

// bool comparePointer(std::shared_ptr<FileSystemComponent> a, std::shared_ptr<FileSystemComponent> b) {
//     if (a->name.compare(b->name)){
//         return true;
//     }
//     else {
//         return false;
//     }
// }

void RmFileCommand ::executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem) {
#define CHILD filesystem->currentWorkingDir->child

    // std::sort(CHILD.begin(), CHILD.end(), comparePointer);
    // for (auto i: CHILD){
    //     cout << i->name << endl;
    // }

    bool isFileFound = false;

    if (arguments.size() < 2) {
        cout << "rmdir: missing operand" << endl;
        return;
    }

    // ! Rewrite Algo to reduce the complexity.
    for (int j = 1; j < arguments.size(); j++) {
        for (int i = 0; i < CHILD.size(); i++) {
            if (CHILD[i]->name == arguments[j]) {
                if (CHILD[i]->componentType != _FILE) {
                    cout << arguments[j] << " is not a file" << endl;
                    continue;
                };

                isFileFound = true;
                CHILD.erase(CHILD.begin() + i);
                cout << arguments[j] << " removed " << endl;
            }
        }
        if (!isFileFound) {
            cout << arguments[j] << " not found" << endl;
        }
    }
}