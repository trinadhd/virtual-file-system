#include "Command.hpp"
#include <vector>
#include <iostream>

class ListDirCommand : public ICommand {
public:
    void executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> fs);
};
