#pragma once

#include <iostream>
#include <chrono>
#include "ComponentType.h"

class ISystemComponent : {
    private:
        string name;
        long int size;
        DateTime timeStamp;
        ComponentType type;
        ISystemComponent *parent;

    public:
        // Constructor of class
        ISystemComponent(ComponentType componentType, string componentName);

        // returns the component name
        string RetrieveName();

        // returns the component size
        long int RetrieveSize();

        // Gets the component date time information
        DateTime RetrieveTimeStamp();

        // Gets the component type;
        ComponentType RetrieveType();

        // Load the component state from storage
        virtual void RestoreStateFromStorage() = 0;

        // Saves the component state to storage
        virtual void PersistStateToStorage() = 0;

        // Destructor of class
        virtual ~ISystemComponent();
}