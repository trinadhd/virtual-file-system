#pragma once

#include <iostream>
#include "CommandParser.h"

class ICmdParser : {
    private:
        Command SystemCommand;
        std::unique_ptr<std::vector<std::string>> cmdArguments;

    public:
        // Constructor of class
        ICmdParser(std::vector<std::string> arguments);

        // Obtains the VFS command to execute i.e. Mkdir, ls, etc
        Command RetrieveCommand();

        // Obtain the list of arguments associated with the comand i.e. "mkdir foldername"
        // or empty if the command is "ls" or "ls -a"
        std::unique_ptr<std::vector<std::string>>& RetrieveCommandArgs();

        // Destructor of class
        virtual ~ICmdParser();
}