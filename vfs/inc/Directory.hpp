#pragma once

#include <iostream>
#include <chrono>
#include <vector>
#include "ComponentType.h"

class Directory : public ISystemComponent {
    private:
        string name;
        long int size;
        DateTime timeStamp;
        ComponentType type;
        vector<ISystemComponent> items;

    public:
        // Constructor of class
        Directory(string componentName);
        
        // Destructor of class
        virtual ~Directory();
}