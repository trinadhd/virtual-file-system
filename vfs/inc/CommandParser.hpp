#ifndef COMMAND_PARSER_HPP
#define COMMAND_PARSER_HPP

#include <iostream>
#include <memory>
#include <vector>

#include "FileSystem.hpp"

class ICommandParser {
public:
    virtual void parseCommand(std::string cmd) = 0;
    virtual void setFileSystem(shared_ptr<FileSystem> fs)= 0;
};

class CommandParser : public ICommandParser {
private:
    shared_ptr<FileSystem> fileSystem_;
public:
    CommandParser();
    ~CommandParser();

    void setFileSystem(shared_ptr<FileSystem> filesystem) override;
    void parseCommand(std::string cmd) override;
};

#endif