#include "Command.hpp"
#include <vector>

class RmDirCommand : public ICommand {
public:
    void executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> fs);
};
