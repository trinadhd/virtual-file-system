#pragma once

#include <iostream>
#include <chrono>
#include "ComponentType.h"

class File : public ISystemComponent {
    private:
        string name;
        long int size;
        DateTime timeStamp;
        ComponentType type;

    public:
        // Constructor of class
        File(string componentName)
        
        // Destructor of class
        virtual ~File();
}