#pragma once

#include <iostream>
#include <vector>
#include "ComponentType.h"
#include "ICmdParser.h"

class IFileSystem : {
    private:
        std::vector<ISystemComponent> fileSystem;

    public:
        // Constructor of class
        IFileSystem();

        // loads file system state from storage
        void RestoreStateFromStorage();

        // Saves file system state to storage
        void PersistStateToStorage();

        // Executes the given command
        void ExecuteCommand(ICmdParser command);

        // Returns a component from the system by name and type
        ISystemComponent& RetrieveComponent(std:string componentName, ComponentType componentType);

        // Destructor of class
        virtual ~IFileSystem();
}