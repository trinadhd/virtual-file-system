#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP

#include <iostream>

#include "FileSystemComponent.hpp"

using namespace std;

class FileSystem {
public:
    FileSystem();
    ~FileSystem();
    shared_ptr<FileSystemComponent> rootDir, currentWorkingDir;
};

#endif




































// class IFileSystemComponent {
// public:
//     virtual void    executeCommand() = 0;
//     virtual void    setSize(int16_t size) = 0;
//     virtual int16_t getSize() = 0;
//     virtual void    setName(string name) = 0;
//     virtual string  getName() = 0;
// };

// class FileSystemComponent : public IFileSystemComponent {
//     string                                  name_;
//     int16_t                                 size_;
//     shared_ptr<FileSystemComponent>         parent;
//     vector<shared_ptr<FileSystemComponent>> children;

// public:
//     FileSystemComponent();
//     ~FileSystemComponent();
//     void                                    executeCommand();
//     string                                  getName();
//     void                                    setName(string name);
//     int16_t                                 getSize();
//     void                                    setSize(int16_t size);
//     // unique_ptr<IFileSystemComponent>&       operator=(unique_ptr<IFileSystemComponent>&) = delete;
//     bool                                    setParent(shared_ptr<FileSystemComponent>);
//     shared_ptr<FileSystemComponent>         getParent();
//     bool                                    addChildren(shared_ptr<FileSystemComponent>);
//     vector<shared_ptr<FileSystemComponent>> getChildren();
// };

// class IFileSystem {
// public:
//     virtual std::shared_ptr<IFileSystemComponent> getRootDirectory() = 0;
//     //virtual void setRootDirectory(std::unique_ptr<IFileSystemComponent> rootDir)=0;
// };

// class FileSystem : public IFileSystem {
//     std::shared_ptr<FileSystemComponent> rootDirectory_;
//     std::shared_ptr<FileSystemComponent> currentWorkingDirectory;

// public:
//     FileSystem();
//     ~FileSystem();
//     void                                  executeCommand();
//     std::shared_ptr<IFileSystemComponent> getRootDirectory();
//     std::shared_ptr<IFileSystemComponent> getCurrentWorkingDirectory();
//     bool                                  setRootDirectory();
//     bool                                  setCurrentWorkingDirectory(shared_ptr<FileSystemComponent> cwd);
//     //void  setRootDirectory(std::unique_ptr<IFileSystemComponent> fsc);
// };