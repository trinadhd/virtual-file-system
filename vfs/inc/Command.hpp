////////////////////////////////////////////////////////////////////////////////

/**
 
 *  @file   Command.hpp
 
 *  @brief  

 *  Contains enumerator listing the supported file system comands
 * 
 
 * (C) Copyright 2018 HP Development Company, L.P.
 
 *  All rights reserved.
 
 */

////////////////////////////////////////////////////////////////////////////////
#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "ComponentType.hpp"
#include "FileSystem.hpp"

class ICommand {
public:
    virtual void executeCommand(std::vector<std::string> const arguments, shared_ptr<FileSystem> filesystem)=0;
};



extern std::map<std::string, ICommand*>
    Map;

#endif