#ifndef _EXAMPLE_HPP
#define _EXAMPLE_HPP
#include <iostream>
#include <list>
using namespace std;
int MAC(int x, int y, int& sum);
int Square(int x);

class MyClass
{

	string _id;

	public:
	MyClass(string id); 
	string GetId();
	int getIdLen();

};


class File
{
	int handle;
	public:
		virtual int open(string filename)=0;
		virtual int write( char* buff, int size)=0;
//		virtual int read(int handle, char * buff, int size);

};


class MyApp
{

	File& _file;

	public :
		MyApp(File& file); 
		int Init(string filename);
		int WriteData(char* data);

};


class NotificationService
{	
	public :
	void virtual send(string msg)=0;

};

class BankAccount 
{
	int _balance;
	NotificationService *_ns;	
	public :
		BankAccount();
		BankAccount(int balance);
		int getBalance();
		void deposit(int amount);
		void setMailer(NotificationService *ns);
		int withdraw(int amount);
};


#endif 
